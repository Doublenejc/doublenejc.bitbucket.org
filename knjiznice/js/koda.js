
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}

function kreirajEHRzaBolnika(){
  {
    
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();
  
  

  

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId
            
          }
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}
  
}
function preberiEHRodBolnika() {
  
  
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames +" "+
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
	}
}
function tezava(){
  var a=document.getElementById("kreirajTezavo");
  a=a.value;
  //$("#mesic").html(a);
 
  if(a=="cardiologist"){
    
    
    var ourRequest=new XMLHttpRequest();
    ourRequest.open('GET','https://api.betterdoctor.com/2016-03-01/doctors?specialty_uid=cardiologist&location=37.773%2C-122.413%2C100&user_location=37.773%2C-122.413&sort=best-match-desc&skip=0&limit=1&user_key=889685745e05bea4f95c1e71a01d1af2');
    ourRequest.onload= function(){
      var ourData=JSON.parse(ourRequest.responseText);
      $("#mesic").html("Najprimernejši zdravnik za vas je "+a+" "+ourData.data[0].profile.first_name+" "+ourData.data[0].profile.last_name+". Telefonska stevilka:"+ourData.data[0].practices[0].phones[0].number+". Na naslovu "+ourData.data[0].practices[0].visit_address.street+", "+ourData.data[0].practices[0].visit_address.city+".");
      
      
    };
    ourRequest.send();
    
    
  }else if(a=="neurologist"){
    
    var ourRequest=new XMLHttpRequest();
    ourRequest.open('GET','https://api.betterdoctor.com/2016-03-01/doctors?specialty_uid=neurologist&location=37.773%2C-122.413%2C100&user_location=37.773%2C-122.413&sort=best-match-desc&skip=0&limit=1&user_key=889685745e05bea4f95c1e71a01d1af2');
    ourRequest.onload= function(){
      var ourData=JSON.parse(ourRequest.responseText);
      $("#mesic").html("Najprimernejši zdravnik za vas je "+a+" "+ourData.data[0].profile.first_name+" "+ourData.data[0].profile.last_name+". Telefonska stevilka:"+ourData.data[0].practices[0].phones[0].number+". Na naslovu "+ourData.data[0].practices[0].visit_address.street+", "+ourData.data[0].practices[0].visit_address.city+".");
      
      
    };
    ourRequest.send();
  }else if(a=="nephrologist"){
    var ourRequest=new XMLHttpRequest();
    ourRequest.open('GET','https://api.betterdoctor.com/2016-03-01/doctors?specialty_uid=nephrologist&location=37.773%2C-122.413%2C100&user_location=37.773%2C-122.413&sort=best-match-desc&skip=0&limit=1&user_key=889685745e05bea4f95c1e71a01d1af2');
    ourRequest.onload= function(){
      var ourData=JSON.parse(ourRequest.responseText);
      $("#mesic").html("Najprimernejši zdravnik za vas je "+a+" "+ourData.data[0].profile.first_name+" "+ourData.data[0].profile.last_name+". Telefonska stevilka:"+ourData.data[0].practices[0].phones[0].number+". Na naslovu "+ourData.data[0].practices[0].visit_address.street+", "+ourData.data[0].practices[0].visit_address.city+".");
      
      
    };
    ourRequest.send();
  }else if(a=="urologist"){
    
    var ourRequest=new XMLHttpRequest();
    ourRequest.open('GET','https://api.betterdoctor.com/2016-03-01/doctors?specialty_uid=urologist&location=37.773%2C-122.413%2C100&user_location=37.773%2C-122.413&sort=best-match-desc&skip=0&limit=1&user_key=889685745e05bea4f95c1e71a01d1af2');
    ourRequest.onload= function(){
      var ourData=JSON.parse(ourRequest.responseText);
      $("#mesic").html("Najprimernejši zdravnik za vas je "+a+" "+ourData.data[0].profile.first_name+" "+ourData.data[0].profile.last_name+". Telefonska stevilka:"+ourData.data[0].practices[0].phones[0].number+". Na naslovu "+ourData.data[0].practices[0].visit_address.street+", "+ourData.data[0].practices[0].visit_address.city+".");
      
      
    };
    ourRequest.send();
    
  }else if(a=="ophthalmologist"){
    var ourRequest=new XMLHttpRequest();
    ourRequest.open('GET','https://api.betterdoctor.com/2016-03-01/doctors?specialty_uid=ophthalmologist&location=37.773%2C-122.413%2C100&user_location=37.773%2C-122.413&sort=best-match-desc&skip=0&limit=1&user_key=889685745e05bea4f95c1e71a01d1af2');
    ourRequest.onload= function(){
      var ourData=JSON.parse(ourRequest.responseText);
      $("#mesic").html("Najprimernejši zdravnik za vas je "+a+" "+ourData.data[0].profile.first_name+" "+ourData.data[0].profile.last_name+". Telefonska stevilka:"+ourData.data[0].practices[0].phones[0].number+". Na naslovu "+ourData.data[0].practices[0].visit_address.street+", "+ourData.data[0].practices[0].visit_address.city+".");
      
      
    };
    ourRequest.send();
  }else if(a=="dermatologist"){
    var ourRequest=new XMLHttpRequest();
    ourRequest.open('GET','https://api.betterdoctor.com/2016-03-01/doctors?specialty_uid=dermatologist&location=37.773%2C-122.413%2C100&user_location=37.773%2C-122.413&sort=best-match-desc&skip=0&limit=1&user_key=889685745e05bea4f95c1e71a01d1af2');
    ourRequest.onload= function(){
      var ourData=JSON.parse(ourRequest.responseText);
      $("#mesic").html("Najprimernejši zdravnik za vas je "+a+" "+ourData.data[0].profile.first_name+" "+ourData.data[0].profile.last_name+". Telefonska stevilka:"+ourData.data[0].practices[0].phones[0].number+". Na naslovu "+ourData.data[0].practices[0].visit_address.street+", "+ourData.data[0].practices[0].visit_address.city+".");
      
      
    };
    ourRequest.send();
  }else if(a=="pulmonologist"){
    var ourRequest=new XMLHttpRequest();
    ourRequest.open('GET','https://api.betterdoctor.com/2016-03-01/doctors?specialty_uid=pulmonologist&location=37.773%2C-122.413%2C100&user_location=37.773%2C-122.413&sort=best-match-desc&skip=0&limit=1&user_key=889685745e05bea4f95c1e71a01d1af2');
    ourRequest.onload= function(){
      var ourData=JSON.parse(ourRequest.responseText);
      $("#mesic").html("Najprimernejši zdravnik za vas je "+a+" "+ourData.data[0].profile.first_name+" "+ourData.data[0].profile.last_name+". Telefonska stevilka:"+ourData.data[0].practices[0].phones[0].number+". Na naslovu "+ourData.data[0].practices[0].visit_address.street+", "+ourData.data[0].practices[0].visit_address.city+".");
      
      
    };
    ourRequest.send();
  }else if(a=="psychiatrist"){
    var ourRequest=new XMLHttpRequest();
    ourRequest.open('GET','https://api.betterdoctor.com/2016-03-01/doctors?specialty_uid=psychiatrist&location=37.773%2C-122.413%2C100&user_location=37.773%2C-122.413&sort=best-match-desc&skip=0&limit=1&user_key=889685745e05bea4f95c1e71a01d1af2');
    ourRequest.onload= function(){
      var ourData=JSON.parse(ourRequest.responseText);
      $("#mesic").html("Najprimernejši zdravnik za vas je "+a+" "+ourData.data[0].profile.first_name+" "+ourData.data[0].profile.last_name+". Telefonska stevilka:"+ourData.data[0].practices[0].phones[0].number+". Na naslovu "+ourData.data[0].practices[0].visit_address.street+", "+ourData.data[0].practices[0].visit_address.city+".");
      
      
    };
    ourRequest.send();
  }
  
  
  
  
  
  
  
  
}
$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});
// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
